function createNewUser() {

	let newUser = {
		userName: prompt("Введіть ваше ім'я: ",''),
		userSecondName: prompt("Введіть ваше прізвище ",''),
        Birthday: prompt("Введіть свою дату народження у форматі: дд.мм.рррр "),
		
		getPassword() {
			let newLogin = this.userName.charAt(0).toUpperCase() + this.userName.substring(1) + ' ' + this.userSecondName.charAt(0).toUpperCase() + this.userSecondName.substring(1);
			return newLogin;
		},

        getLogin() {
			let newLogin = this.userName.charAt(0).toUpperCase() + this.userSecondName.toLowerCase();
            return newLogin;
		},

		setFirstName(newFirstName) {
			Object.defineProperty(this, "userName", { value: newFirstName });
		},

		setLastName(newLastName) {
			Object.defineProperty(this, "userSecondName", { value: newLastName });
		},

        getAge() {
			let age = (new Date() - new Date(this.Birthday.substring(6, 10), this.Birthday.substring(3, 5) - 1, this.Birthday.substring(0, 2)))/ (1000 * 60 * 60 * 24 * 365.25);
			return Math.floor(age);
		},

        setAge(newBirthday) {
			Object.defineProperty(this, "Birthday", { value: newBirthday });
		},
        
	};

    Object.defineProperty(newUser, "userName", {
        configurable: true,
        writable: false,
    });
    Object.defineProperty(newUser, "userSecondName", {
        configurable: true,
        writable: false,
    });
    Object.defineProperty(newUser, "Birthday", {
        configurable: true,
        writable: false,
    });
    
	return newUser;
    
}

let newUserObj = new createNewUser();

console.log (`${newUserObj.getPassword()} ${newUserObj.getAge()} → ${newUserObj.getLogin()} ${newUserObj.getAge()}`);